<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'pageHome']);
Route::get('/register', [AuthController::class, 'pageRegister']);
Route::post('/welcome', [AuthController::class, 'pageWelcome']);
Route::get('/data-tables', [AuthController::class, 'pageDataTables']);
Route::get('/table', [AuthController::class, 'pageTables']);


//CAST ROUTES
Route::controller(CastController::class)->group(function () {
    Route::get('/cast', 'index');//menampilkan data cast
    Route::get('/cast/create', 'create'); //form create cast
    Route::post('/cast', 'store'); //function post create cast
    Route::get('/cast/{cast_id}', 'show'); //menampilkan detail cast berdasarkan id
    Route::get('/cast/{cast_id}/edit', 'edit');
    Route::put('/cast/{cast_id}', 'update');
    Route::delete('/cast/{cast_id}', 'destroy'); //delete cast berdasarkan id
});

