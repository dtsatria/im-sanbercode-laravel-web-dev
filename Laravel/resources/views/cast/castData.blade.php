@extends('master')

@section('judul')
Data Table
@endsection

@push('scripts')
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
<script src="{{asset('/template/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
@endpush

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>  
@endpush

@section('content')
<a href="/cast/create" class="btn btn-primary btn-sm mb-2" aria-pressed="true">Create Cast</a>

<table id="example1" class="table table-bordered table-striped">
    <thead>
    <tr>
      <th>Name</th>
      <th>Bio</th>
      <th>Umur</th>
      <th>Action</th>
    </tr>
    </thead>
    <tbody>
      @forelse ($cast as $data)
        <tr>
          <td>{{$data->name}}</td>
          <td>{{$data->bio}}</td>
          <td>{{$data->umur}}</td>
          <td>
            <form action="/cast/{{$data->id}}" method="POST">
              @csrf
              @method('DELETE')
              <a href="/cast/{{$data->id}}" class="btn btn-info btn-sm">Detail</a>
              <a href="/cast/{{$data->id}}/edit" class="btn btn-info btn-sm">Edit/Update</a>
            <input type="submit" value="Delete" class="btn btn-danger btn-sm"></input>

            </form>
          </td>
        </tr>
      @empty
      <tr>
        <td>No Cast</td>
      </tr>
      @endforelse
    </tbody>
    <tfoot>
    <tr>
        <th>Name</th>
        <th>Bio</th>
        <th>Umur</th>
        <th>Action</th>
    </tr>
    </tfoot>
  </table>
@endsection
