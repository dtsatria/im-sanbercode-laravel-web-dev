@extends('master')

@section('judul')
Edit Cast
@endsection


@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label >Name</label>
      <input type="text" name="name" value="{{$cast->name}}" class="form-control" placeholder="Enter Name">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio</label>
        <textarea class="form-control" name="bio" rows="3" placeholder="Your Bio Here">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Age</label>
        <input type="text" class="form-control" value="{{$cast->umur}}" name="umur" placeholder="How Old Are You">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/cast" class="btn btn-primary">Back</a>

    
  </form>
@endsection
