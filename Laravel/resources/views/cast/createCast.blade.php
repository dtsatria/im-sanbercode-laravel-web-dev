@extends('master')

@section('judul')
Create Cast
@endsection


@section('content')
<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
      <label >Name</label>
      <input type="text" name="name" class="form-control" placeholder="Enter Name">
    </div>
    @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Bio</label>
        <textarea class="form-control" name="bio" rows="3" placeholder="Your Bio Here"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Age</label>
        <input type="text" class="form-control" name="umur" placeholder="How Old Are You">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="/cast" class="btn btn-primary">Back</a>

    
  </form>
@endsection
