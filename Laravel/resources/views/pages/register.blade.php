@extends('master')

@section('judul')
<h3>Sign Up Form</h3>
@endsection

@section('content')

<h1>Buat Account Baru!</h1>

<form action="/welcome" method="POST">
    @csrf
    <label for="fname">First name:</label><br><br>
    <input type="text" id="fname" name="fname"><br><br>
    <label for="lname">Last name:</label><br><br>
    <input type="text" id="lname" name="lname"><br>

    <br>
    <label for="gender">Gender :</label><br><br>
    <input type="radio" id="gender" name="gender" value="Male">
    <label for="html">Male</label><br>
    <input type="radio" id="gender" name="gender" value="Female">
    <label for="css">Female</label><br>
    <input type="radio" id="gender" name="gender" value="Other">
    <label for="javascript">Other</label>

    <br>
    <br>
    <label for="nation">Nationality :</label><br><br>
    <select id="nation" name="nation">
        <option value="Indonesia">Indonesia</option>
        <option value="Malaysia">Malaysia</option>
        <option value="Singapore">Singapore</option>
    </select>

    <br>
    <br>
    <label for="language">Language Spoken :</label><br><br>
    <input type="checkbox" id="Bahasa" name="language" value="Bahasa">
        <label for="language"> Bahasa Indonesia</label><br>
        <input type="checkbox" id="English" name="language" value="English">
        <label for="language"> English</label><br>
        <input type="checkbox" id="Other" name="language" value="Other">
        <label for="language"> Other</label>


    <br>
    <br>
    <label for="bio">Bio :</label><br><br>
        <textarea name="message" rows="10" cols="30"></textarea>
    <br>
        
        <input type="submit" value="Sign Up">
</form>
@endsection
