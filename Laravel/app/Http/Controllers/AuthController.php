<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function pageRegister()
    {
        return view('pages.register');
    }

    public function pageWelcome(Request $request)
    {
        $fname = $request['fname'];
        $lname = $request['lname'];


        return view('pages.welcome', ['fname' => $fname, 'lname' => $lname]);
    }

    public function pageDataTables()
    {
        return view('pages.dataTables');
    }

    public function pageTables()
    {
        return view('pages.tables');
    }
}
