<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dika Trisna Satria</title>
</head>
<body>
    <?php
    require('Animal.php');
    require('Frog.php');
    require('Ape.php');

    echo "<h2>Tugas OOP PHP</h2>";
    echo "<h3>Dika Trisna Satria</h3>";

    echo "<h3>RELEASE 0 </h3>";

    //Instansiasi Animal
    $sheep = new Animal("shaun");
    echo "Name : " . $sheep->getName() . "<br>";
    echo "Legs : " . $sheep->getLegs() . "<br>";
    echo "Cold Blooded : " . $sheep->getColdBlooded() . "<br>";


    echo "<h3>RELEASE 1 </h3>";
    echo "Name : " . $sheep->getName() . "<br>";
    echo "Legs : " . $sheep->getLegs() . "<br>";
    echo "Cold Blooded : " . $sheep->getColdBlooded() . "<br>";
    echo "<br>";
    
    //Instansiasi Kodok
    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->getName() . "<br>";
    echo "Legs : " . $kodok->getLegs() . "<br>";
    echo "Cold Blooded : " . $kodok->getColdBlooded() . "<br>";
    echo "Jump : " . $kodok->jump();
    echo "<br>";
    echo "<br>";

    //Instansiasi Ape
    $sungokong = new Ape("kera sakti");
    $sungokong->setLegs(2);
    echo "Name : " . $sungokong->getName() . "<br>";
    echo "Legs : " . $sungokong->getLegs() . "<br>";
    echo "Cold Blooded : " . $sungokong->getColdBlooded() . "<br>";
    echo "Yell : " . $sungokong->yell();

    ?>
    
</body>
</html>