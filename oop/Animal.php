<?php
class Animal {
    
    protected string $name;
    protected int $legs = 4;
    protected string $cold_blooded = 'no';

    //Constructor
    public function __construct($name) 
  {
    $this->name = $name;
  }

  //GETTER DAN SETTER ANIMAL START
  public function setName(string $name) : void {
    $this-> name = $name;
  }

  public function getName() : string {
    return $this->name;
  }

  public function setLegs(int $legs) : void {
    $this-> legs = $legs;
  }

  public function getLegs() : int {
    return $this->legs;
  }

  public function setColdBlooded(string $cold_blooded) : void {
    $this-> cold_blooded = $cold_blooded;
  }

  public function getColdBlooded() : string {
    return $this->cold_blooded;
  }
  //GETTER DAN SETTER ANIMAL END

  
}
?>
